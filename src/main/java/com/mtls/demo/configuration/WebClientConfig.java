package com.mtls.demo.configuration;

import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import javax.net.ssl.KeyManagerFactory;
import java.io.FileInputStream;
import java.security.KeyStore;

@Configuration
public class WebClientConfig {

    private static final String PKCS12_FILE_PATH = "src/main/resources/static/badssl.com-client.p12";
    private static final String PKCS12_PASSWORD = "badssl.com";

    @Bean
    public WebClient webClient() {
        return WebClient.builder()
                .clientConnector(getReactorClientHttpConnector())
                .exchangeStrategies(ExchangeStrategies.builder()
                        .build()).build();
    }

    private ClientHttpConnector getReactorClientHttpConnector() {
        return new ReactorClientHttpConnector(HttpClient.create()
                .secure(sslContextSpec -> sslContextSpec.sslContext(configureSslContext())));
    }


    public static SslContext configureSslContext() {
        try {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(new FileInputStream(PKCS12_FILE_PATH), PKCS12_PASSWORD.toCharArray());

            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(keyStore, PKCS12_PASSWORD.toCharArray());

            return SslContextBuilder.forClient()
                    .keyManager(keyManagerFactory)
                    .build();
        } catch (Exception e) {
            throw new RuntimeException("Error configuring SslContext", e);
        }
    }
}
