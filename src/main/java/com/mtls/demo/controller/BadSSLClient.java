package com.mtls.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
@Slf4j
public class BadSSLClient {

    @Autowired
    private WebClient webClient;

    @GetMapping("/send-request")
    public Mono<String> sendRequest() {
        log.info("Sending request to badssl.com");
        String url = "https://client.badssl.com/";
        log.info(STR."URL: \{url}");
        Mono<String> response = webClient.get().uri(url).retrieve().bodyToMono(String.class);
        log.info(STR."Response: \{response.block()}");
        return response;
    }
}